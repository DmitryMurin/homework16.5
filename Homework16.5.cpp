#include <iostream>
#include <ctime>

int main()
{
    const int N = 5;
    int array[N][N];

    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            array[i][j] = i + j;
        }
    }
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            std::cout << array[i][j] << " ";
        }
        std::cout << std::endl;
    }
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int currentDay = buf.tm_mday;

    int SumLine = currentDay % N;

    int sum = 0;
    for (int j = 0; j < N; ++j)
    {
        sum += array[SumLine][j];
    }
    std::cout << "sum of the string numbers " << SumLine << "= " << sum << std::endl;

    return 0;
}
